<?php
/**
 * The template for displaying the style-2 footer layout.
 *
 * @package Contractor
 */

?>
<div class="footer-container <?php echo contractor_get_invert_class_customize_option( 'footer_bg' ); ?>">
	<div class="site-info container">
		<?php
			contractor_footer_logo();
			contractor_footer_menu();
			contractor_contact_block( 'footer' );
			contractor_social_list( 'footer' );
			contractor_footer_copyright();
		?>
	</div><!-- .site-info -->
</div><!-- .container -->
