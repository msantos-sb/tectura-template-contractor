<?php
/**
 * Template part for default header layout.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Contractor
 */
?>
<div class="header-container_wrap container">
	<div class="header-container__flex">
		<div class="site-branding">
			<?php contractor_header_logo() ?>
			<?php contractor_site_description(); ?>
		</div>

		<?php contractor_main_menu(); ?>
		<?php contractor_header_btn(); ?>

	</div>
</div>
