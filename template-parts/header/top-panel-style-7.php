<?php
/**
 * Template part for top panel in header (style-7 layout).
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Contractor
 */

// Don't show top panel if all elements are disabled.
if ( ! contractor_is_top_panel_visible() ) {
	return;
} ?>

<div class="top-panel <?php echo contractor_get_invert_class_customize_option( 'top_panel_bg' ); ?>">
	<div class="top-panel__container container">
		<div class="top-panel__top">
			<div class="top-panel__left">
				<?php contractor_top_message( '<div class="top-panel__message">%s</div>' ); ?>
				<?php contractor_contact_block( 'header' ); ?>
			</div>

			<div class="top-panel__right">
				<?php contractor_top_menu(); ?>
				<?php contractor_social_list( 'header' ); ?>
				<?php contractor_header_search( '<div class="header-search"><span class="search-form__toggle"></span>%s<span class="search-form__close"></span></div>' ); ?>
				<?php contractor_header_woo_elements(); ?>
			</div>
		</div>
	</div>
</div><!-- .top-panel -->
