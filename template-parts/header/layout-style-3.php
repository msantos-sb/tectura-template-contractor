<?php
/**
 * Template part for style-3 header layout.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Contractor
 */
?>
<div class="header-container_wrap container">
	<?php contractor_vertical_main_menu(); ?>
	<div class="header-container__flex">
		<div class="site-branding">
			<?php contractor_header_logo() ?>
			<?php contractor_site_description(); ?>
		</div>

		<div class="header-icons">
			<?php contractor_header_search( '<div class="header-search"><span class="search-form__toggle"></span>%s<span class="search-form__close"></span></div>' ); ?>
			<?php contractor_header_woo_elements(); ?>
			<?php contractor_vertical_menu_toggle( 'main-menu' ); ?>
			<?php contractor_header_btn(); ?>
		</div>

	</div>
</div>
