<?php
/**
 * Menus configuration.
 *
 * @package Contractor
 */

add_action( 'after_setup_theme', 'contractor_register_menus', 5 );
/**
 * Register menus.
 */
function contractor_register_menus() {

	register_nav_menus( array(
		'top'          => esc_html__( 'Top', 'contractor' ),
		'main'         => esc_html__( 'Main', 'contractor' ),
		'main_landing' => esc_html__( 'Landing Main', 'contractor' ),
		'footer'       => esc_html__( 'Footer', 'contractor' ),
		'social'       => esc_html__( 'Social', 'contractor' ),
	) );
}
