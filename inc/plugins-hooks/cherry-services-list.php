<?php
/**
 * Cherry-services-list hooks.
 *
 * @package Contractor
 */

// Customization cherry-services-list plugin.
add_filter( 'cherry_services_list_meta_options_args', 'contractor_change_services_list_icon_pack' );
add_filter( 'cherry_services_default_icon_format', 'contractor_cherry_services_default_icon_format' );
add_filter( 'cherry_services_features_title_format', 'contractor_cherry_services_features_title_format' );

/**
 * Change cherry-services-list icon pack.
 */
function contractor_change_services_list_icon_pack( $fields ) {

	$fields['fields']['cherry-services-icon']['icon_data'] = array(
		'icon_set'    => 'contractorLinearIcons',
		'icon_css'    => CONTRACTOR_THEME_URI . '/assets/css/linearicons.css',
		'icon_base'   => 'linearicon',
		'icon_prefix' => 'linearicon-',
		'icons'       => contractor_get_linear_icons_set(),
	);

	return $fields;
}

/**
 * Change cherry-services-list icon format
 *
 * @return string
 */
function contractor_cherry_services_default_icon_format( $icon_format ) {
	return '<i class="linearicon %s"></i>';
}

/**
 * Change cherry-services features title format.
 */
function contractor_cherry_services_features_title_format( $title_format ) {
	return '<h5 class="service-features_title">%s</h5>';
}

