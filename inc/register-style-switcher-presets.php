<?php
/**
 * Register presets for TM Style Switcher
 *
 * @package Contractor
 */
if ( function_exists( 'tmss_register_preset' ) ) {

	tmss_register_preset(
		'default',
		esc_html__( 'Contractor', 'contractor' ),
		get_stylesheet_directory_uri() . '/tm-style-switcher-pressets/default.png',
		get_stylesheet_directory() . '/tm-style-switcher-pressets/default.json'
	);

	tmss_register_preset(
		'skin1',
		esc_html__( 'Construction', 'contractor' ),
		get_stylesheet_directory_uri() . '/tm-style-switcher-pressets/skin1.png',
		get_stylesheet_directory() . '/tm-style-switcher-pressets/skin1.json'
	);

	tmss_register_preset(
		'skin2',
		esc_html__( 'Fashion', 'contractor' ),
		get_stylesheet_directory_uri() . '/tm-style-switcher-pressets/skin2.png',
		get_stylesheet_directory() . '/tm-style-switcher-pressets/skin2.json'
	);

	tmss_register_preset(
		'skin3',
		esc_html__( 'Furniture', 'contractor' ),
		get_stylesheet_directory_uri() . '/tm-style-switcher-pressets/skin3.png',
		get_stylesheet_directory() . '/tm-style-switcher-pressets/skin3.json'
	);

	tmss_register_preset(
		'skin4',
		esc_html__( 'Ironmass', 'contractor' ),
		get_stylesheet_directory_uri() . '/tm-style-switcher-pressets/skin4.png',
		get_stylesheet_directory() . '/tm-style-switcher-pressets/skin4.json'
	);

	tmss_register_preset(
		'skin5',
		esc_html__( 'Modern', 'contractor' ),
		get_stylesheet_directory_uri() . '/tm-style-switcher-pressets/skin5.png',
		get_stylesheet_directory() . '/tm-style-switcher-pressets/skin5.json'
	);

	tmss_register_preset(
		'skin6',
		esc_html__( 'Resto', 'contractor' ),
		get_stylesheet_directory_uri() . '/tm-style-switcher-pressets/skin6.png',
		get_stylesheet_directory() . '/tm-style-switcher-pressets/skin6.json'
	);

	tmss_register_preset(
		'skin7',
		esc_html__( 'LoanOffer', 'contractor' ),
		get_stylesheet_directory_uri() . '/tm-style-switcher-pressets/skin7.png',
		get_stylesheet_directory() . '/tm-style-switcher-pressets/skin7.json'
	);

	tmss_register_preset(
		'skin8',
		esc_html__( 'Corporate', 'contractor' ),
		get_stylesheet_directory_uri() . '/tm-style-switcher-pressets/skin8.png',
		get_stylesheet_directory() . '/tm-style-switcher-pressets/skin8.json'
	);

	tmss_register_preset(
		'skin9',
		esc_html__( 'Lawyer', 'contractor' ),
		get_stylesheet_directory_uri() . '/tm-style-switcher-pressets/skin9.png',
		get_stylesheet_directory() . '/tm-style-switcher-pressets/skin9.json'
	);
}
