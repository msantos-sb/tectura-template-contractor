<?php get_header( contractor_template_base() ); ?>

	<?php contractor_site_breadcrumbs(); ?>

	<div <?php contractor_content_wrap_class(); ?>>

		<div class="row">

			<div id="primary" <?php contractor_primary_content_class(); ?>>

				<main id="main" class="site-main" role="main">

					<?php include contractor_template_path(); ?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row -->

	</div><!-- .container -->

<?php get_footer( contractor_template_base() ); ?>
